package com.example.calculadora;

public class Calculator {
    public double add(double op1, double op2){
        return op1 + op2;
    }

    public double substract(double op1, double op2){
        return op1 - op2;
    }

    public double mult(double op1, double op2){
        return op1 * op2;
    }

    public double div(double op1, double op2){
        return op1 / op2;
    }
    public double pow(double op1, double op2){
        return Math.pow(op1, op2);
    }


}
