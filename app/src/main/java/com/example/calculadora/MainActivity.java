package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView textResult;
    private EditText num1;
    private EditText num2;
    private Button btAdd;
    private Button btSubst;
    private Button btMult;
    private Button btDiv;
    private Button btPow;
    private Button btCE;
    private Button btInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Calculator cal = new Calculator();

        textResult = (TextView) findViewById(R.id.textResult);
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
        btAdd = (Button) findViewById(R.id.btAdd);
        btSubst = (Button) findViewById(R.id.btSubst);
        btMult = (Button) findViewById(R.id.btMult);
        btDiv = (Button) findViewById(R.id.btDiv);
        btPow = (Button) findViewById(R.id.btPow);
        btCE = (Button) findViewById(R.id.btCE);
        btInfo = (Button) findViewById(R.id.btInfo);

        textResult.setText("0.0");

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Button Add", Toast.LENGTH_SHORT).show();
                    double op1D = Double.parseDouble(num1.getText().toString().trim());
                    double op2D = Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(cal.add(op1D, op2D) + "");
                }
                /*Toast toast = Toast.makeText(MainActivity.this, "Button Add", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 120);
                toast.show();

                double op1D = Double.parseDouble(num1.getText().toString().trim());
                double op2D = Double.parseDouble(num2.getText().toString().trim());

                textResult.setText(cal.add(op1D, op2D) + "");*/

            }
        });
        btSubst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Button Substract", Toast.LENGTH_SHORT).show();
                    double op1D = Double.parseDouble(num1.getText().toString().trim());
                    double op2D = Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(cal.substract(op1D, op2D) + "");
                }
            }
        });
        btMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Button Multiply", Toast.LENGTH_SHORT).show();
                    double op1D = Double.parseDouble(num1.getText().toString().trim());
                    double op2D = Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(cal.mult(op1D, op2D) + "");
                }
            }
        });
        btDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Button Division", Toast.LENGTH_SHORT).show();
                    double op1D = Double.parseDouble(num1.getText().toString().trim());
                    double op2D = Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(cal.div(op1D, op2D) + "");
                }
            }
        });
        btPow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Button Power", Toast.LENGTH_SHORT).show();
                    double op1D = Double.parseDouble(num1.getText().toString().trim());
                    double op2D = Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(cal.pow(op1D, op2D) + "");
                }
            }
        });
        btCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    num1.setText("Enter a number");
                    num2.setText("Enter a number");
                    textResult.setText("0.0");
            }
        });

        //btInfo.setOnClickListener();

        //Intent intent = new Intent(MainActivity.this, Info.class);
        //startActivity(intent);

        btInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Info.class);
                startActivity(intent);
            }
        });


    }
}